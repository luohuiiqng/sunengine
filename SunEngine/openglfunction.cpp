#include "openglfunction.h"
#include <QObject>

OpenGLFunction::OpenGLFunction()
{    
}

OpenGLFunction::~OpenGLFunction()
{

}

void OpenGLFunction::init()
{
    initializeOpenGLFunctions();
}

void OpenGLFunction::paint()
{     
    glClearColor(m_color.redF(), m_color.greenF(), m_color.blueF(), 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);
}

void OpenGLFunction::resize(int w, int h)
{
    glViewport(0,0,w,h);
}

void OpenGLFunction::setColor(QColor color)
{
    m_color = color;
}
