#include "sunengineopengl.h"
#include <QQuickWindow>
#include <QOpenGLFramebufferObjectFormat>
#include "assist.h"

SunEngineOpenGL::SunEngineOpenGL()
{    
    m_render.init();
}

QOpenGLFramebufferObject *SunEngineOpenGL::createFramebufferObject(const QSize &size){
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    m_render.resize(size.width(),size.height());
    return new QOpenGLFramebufferObject(size, format);
}

void SunEngineOpenGL::render(){
    m_render.paint();
    m_window->update();
}

void SunEngineOpenGL::synchronize(QQuickFramebufferObject *item)
{    
    Assist *pItem = qobject_cast<Assist *>(item);
            if (pItem)
            {
                if (!m_window)
                {
                    m_window = pItem->window();
                }
                m_render.setColor(pItem->getColor());
            }
}

void SunEngineOpenGL::init(QString fragmentSourceCode,QString verSourceCode
                           ,QColor color){
    m_modelData.color = color;
    m_modelData.fragmentSourceCode = fragmentSourceCode;
    m_modelData.verSourceCode = verSourceCode;
    m_modelData.tramsform.fill(1.0f);
}

//Set
void SunEngineOpenGL::rotationReleative(float x,float y,float z){
    m_modelData.rotation.setX(m_modelData.rotation.x()+x);
    m_modelData.rotation.setX(m_modelData.rotation.y()+y);
    m_modelData.rotation.setX(m_modelData.rotation.z()+z);
}
void SunEngineOpenGL::positionReleative(float x,float y,float z){
    m_modelData.position.setX(m_modelData.position.x()+x);
    m_modelData.position.setX(m_modelData.position.y()+y);
    m_modelData.position.setX(m_modelData.position.z()+z);
}
void SunEngineOpenGL::scaleReleative(float x,float y,float z){
    m_modelData.scale.setX(m_modelData.scale.x()+x);
    m_modelData.scale.setX(m_modelData.scale.y()+y);
    m_modelData.scale.setX(m_modelData.scale.z()+z);
}
void SunEngineOpenGL::rotationAbsolute(float x,float y,float z){
    m_modelData.rotation.setX(x);
    m_modelData.rotation.setX(y);
    m_modelData.rotation.setX(z);
}
void SunEngineOpenGL::positionAbsolute(float x,float y,float z){
    m_modelData.position.setX(x);
    m_modelData.position.setX(y);
    m_modelData.position.setX(z);
}
void SunEngineOpenGL::scaleAbsolute(float x,float y,float z){
    m_modelData.scale.setX(x);
    m_modelData.scale.setX(y);
    m_modelData.scale.setX(z);
}
void SunEngineOpenGL::reRotation(){
    m_modelData.rotation.setX(0);
    m_modelData.rotation.setX(0);
    m_modelData.rotation.setX(0);
}
void SunEngineOpenGL::reMoveAbsolute(){
    m_modelData.position.setX(0);
    m_modelData.position.setX(0);
    m_modelData.position.setX(0);
}
void SunEngineOpenGL::reScaleAbsolute(){
    m_modelData.scale.setX(0);
    m_modelData.scale.setX(0);
    m_modelData.scale.setX(0);
}
void SunEngineOpenGL::setColor(QColor color){
    m_modelData.color = color;

}

//Get
QVector3D SunEngineOpenGL::getRotation()const{
    return m_modelData.rotation;
}
QVector3D SunEngineOpenGL::getPosition()const{
    return m_modelData.position;
}
QVector3D SunEngineOpenGL::getScale()const{
    return m_modelData.scale;
}
QColor SunEngineOpenGL::getColor()const{
    return m_modelData.color;
}
