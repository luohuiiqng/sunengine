#include "assist.h"
#include "sunengineopengl.h"

Assist::Assist(QQuickItem * item):QQuickFramebufferObject(item)
{
    //设置坐标系    
    setMirrorVertically(true);        
}

QQuickFramebufferObject::Renderer *Assist::createRenderer() const
{
    return new SunEngineOpenGL();
}



qreal Assist::rx()const{
    return m_rx;
}
qreal Assist::ry()const{
    return m_ry;
}
qreal Assist::rz()const{
    return m_rz;
}
qreal Assist::sx()const{
    return m_sx;
}
qreal Assist::sy()const{
    return m_sy;
}
qreal Assist::sz()const{
    return m_sz;
}
qreal Assist::px()const{
    return m_px;
}
qreal Assist::py()const{
    return m_py;
}
qreal Assist::pz()const{
    return m_pz;
}

int Assist::rgbR()const{
    return m_rgbR;
}
int Assist::rgbG()const{
    return m_rgbG;
}
int Assist::rgbB()const{
    return m_rgbB;
}

void Assist::setRx(qreal value){
    m_rx = value;
    emit rxChanged(value);
}
void Assist::setRy(qreal value){
    m_ry =value;
    update();
    emit ryChanged(value);
}
void Assist::setRz(qreal value){
    m_rz = value;
    emit rzChanged(value);
}
void Assist::setSx(qreal value){
    m_sx = value;
    emit sxChanged(value);
}
void Assist::setSy(qreal value){
    m_sy = value;
    emit syChanged(value);
}
void Assist::setSz(qreal value){
    m_sz = value;
    emit szChanged(value);
}
void Assist::setPx(qreal value){
    m_px = value;
    emit pxChanged(value);
}
void Assist::setPy(qreal value){
    m_py = value;
    emit pyChanged(value);
}
void Assist::setPz(qreal value){
    m_pz = value;
    emit pzChanged(value);
}

void Assist::setRgbR(int value){
    m_rgbR = value;    
    update();
    emit rgbRChanged(value);
}
void Assist::setRgbG(int value){
    m_rgbG = value;
    update();
    emit rgbGChanged(value);
}
void Assist::setRgbB(int value){
    m_rgbB = value;
    update();
    emit rgbBChanged(value);
}


