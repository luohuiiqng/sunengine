#ifndef ASSIST_H
#define ASSIST_H
/***
****通过它在QML中显示控件
***/
#include<QQuickItem>
#include<QObject>
#include<QQuickFramebufferObject>
#include <QTimerEvent>
/*
*https://www.qt.io/blog/2015/05/11/integrating-custom-opengl-rendering-with-qt-quick-via-qquickframebufferobject
*/

class Assist:public QQuickFramebufferObject
{
    Q_OBJECT
public:
    Assist(QQuickItem * item = nullptr);
    /**
    ***重新实现此函数以创建用于渲染到FBO的渲染器。
    ***当GUI线程被阻止时，将在渲染线程上调用此函数。
    **/
    QQuickFramebufferObject::Renderer *createRenderer() const override;
    Q_PROPERTY(qreal rx READ rx WRITE setRx NOTIFY rxChanged);
    Q_PROPERTY(qreal ry READ ry WRITE setRy NOTIFY ryChanged);
    Q_PROPERTY(qreal rz READ rz WRITE setRz NOTIFY rzChanged);
    Q_PROPERTY(qreal sx READ sx WRITE setSx NOTIFY sxChanged);
    Q_PROPERTY(qreal sy READ sy WRITE setSy NOTIFY syChanged);
    Q_PROPERTY(qreal sz READ sz WRITE setSz NOTIFY szChanged);
    Q_PROPERTY(qreal px READ px WRITE setPx NOTIFY pxChanged);
    Q_PROPERTY(qreal py READ py WRITE setPy NOTIFY pyChanged);
    Q_PROPERTY(qreal pz READ pz WRITE setPz NOTIFY pzChanged);
    Q_PROPERTY(int rgbR READ rgbR WRITE setRgbR NOTIFY rgbRChanged);
    Q_PROPERTY(int rgbG READ rgbG WRITE setRgbG NOTIFY rgbGChanged);
    Q_PROPERTY(int rgbB READ rgbB WRITE setRgbB NOTIFY rgbBChanged);
    qreal rx()const;
    qreal ry()const;
    qreal rz()const;
    qreal sx()const;
    qreal sy()const;
    qreal sz()const;
    qreal px()const;
    qreal py()const;
    qreal pz()const;
    int rgbR()const;
    int rgbG()const;
    int rgbB()const;

    void setRx(qreal value);
    void setRy(qreal value);
    void setRz(qreal value);
    void setSx(qreal value);
    void setSy(qreal value);
    void setSz(qreal value);
    void setPx(qreal value);
    void setPy(qreal value);
    void setPz(qreal value);
    void setRgbR(int value);
    void setRgbG(int value);
    void setRgbB(int value);
signals:
    void rxChanged(qreal value);
    void ryChanged(qreal value);
    void rzChanged(qreal value);
    void sxChanged(qreal value);
    void syChanged(qreal value);
    void szChanged(qreal value);
    void pxChanged(qreal value);
    void pyChanged(qreal value);
    void pzChanged(qreal value);
    void rgbRChanged(int value);
    void rgbGChanged(int value);
    void rgbBChanged(int value);
public slots:
public:
     qreal m_rx = 0;
     qreal m_ry = 0;
     qreal m_rz = 0;
     qreal m_sx = 0;
     qreal m_sy = 0;
     qreal m_sz = 0;
     qreal m_px = 0;
     qreal m_py = 0;
     qreal m_pz = 0;
     int m_rgbR = 0;
     int m_rgbG = 0;
     int m_rgbB = 0;   
     QColor getColor()const{
         return QColor(m_rgbR,m_rgbG,m_rgbB);
     }
};

#endif // ASSIST_H
