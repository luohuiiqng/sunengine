import QtQuick
import QtQuick.Controls
import Assist 1.1

Item {
    id: control
    //rotation:
    Rectangle{
        anchors.fill: parent
        color: globalStyle.colorCenter
        Render{
            id: opengl
            anchors.fill: parent
            onRgbBChanged: {
                console.log("--------------")
                console.log("rgbR:",rgbR)
                console.log("rgbG:",rgbG)
                console.log("rgbB:",rgbB)
                console.log("--------------")
            }
        }
        Timer{//定时器
            id:timer
            repeat: true
            interval: 500//毫秒
            running: true
            onTriggered: {
                opengl.rgbR =Math.ceil(Math.random()*255)
                opengl.rgbB =Math.ceil(Math.random()*255)
                opengl.rgbG =Math.ceil(Math.random()*255)
            }
        }
    }
}
