import QtQuick

Window {
    id: main
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    AllContent{
        id:allContent
        anchors.fill: parent
    }

    GlobalStyle{
        id:globalStyle
    }
}
