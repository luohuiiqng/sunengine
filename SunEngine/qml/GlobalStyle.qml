import QtQuick
import QtQuick.Controls

Item {
    id: control

    property color backgroundColor: "skyblue"
    property color borderColor: "gray"
    property color textColor: "white"
    property int borderWidth: 1
    property int rectangleRadius: 2

    property color colorLeft: "orange"
    property color colorRight: "gold"
    property color colorTop: "white"
    property color colorBottom: "#435451"
    property color colorCenter: "pink"
}
