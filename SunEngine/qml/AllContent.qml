import QtQuick

Item {
    id: control

    implicitHeight: 640
    implicitWidth: 720

    Rectangle{
        anchors.fill: parent
        color: globalStyle.backgroundColor
        TopContent{
            id: topContent
            anchors.top: parent.top
            anchors.left: parent.left
            height: 40
            width: parent.width
        }
        LeftContent{
            id:leftContent
            width: 100
            height: parent.height*4/5
            anchors.top: topContent.bottom
            anchors.left: parent.left
        }
        CenterContent{
            id:centerContent
            width: parent.width-200
            height: parent.height*4/5
            anchors.left: leftContent.right
            anchors.top: leftContent.top
        }
        RightContent{
            id:rightContent
            width: 100
            height: parent.height*4/5
            anchors.left: centerContent.right
            anchors.top: centerContent.top
        }
        BottomContent{
            id: bottomContent
            height: parent.height - leftContent.height - topContent.height
            width: parent.width
            anchors.top: rightContent.bottom
            anchors.left: leftContent.left
        }
    }
}
