#ifndef SUNENGINEOPENGL_H
#define SUNENGINEOPENGL_H
#include <QQuickFramebufferObject>
#include <QOpenGLFunctions_3_3_Core>
#include <QMatrix4x4>
#include <QVector3D>
#include <QDebug>
#include "openglfunction.h"

class SunEngineOpenGL: public QQuickFramebufferObject::Renderer
{
public:
    explicit SunEngineOpenGL();
    typedef struct QModel{
        QMatrix4x4 tramsform;//变换矩阵
        QString fragmentSourceCode;//片段着色器源码
        QString verSourceCode;//顶点着色器源码
        QColor color=QColor(12,43,121,255);
        QVector3D position;
        QVector3D rotation;
        QVector3D scale; 
    }QModelTY;
    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size)override;
    void render()override;
    void synchronize(QQuickFramebufferObject *item)override;
public:
    //Set
    void init(QString fragmentSourceCode,QString verSourceCode,QColor color);
    void rotationReleative(float x,float y,float z);
    void positionReleative(float x,float y,float z);
    void scaleReleative(float x,float y,float z);
    void rotationAbsolute(float x,float y,float z);
    void positionAbsolute(float x,float y,float z);
    void scaleAbsolute(float x,float y,float z);
    void reRotation();
    void reMoveAbsolute();
    void reScaleAbsolute();
    void setColor(QColor color);
    //Get
    QVector3D getRotation()const;
    QVector3D getPosition()const;
    QVector3D getScale()const;
    QColor getColor()const;
private:
    QModel m_modelData;
    QQuickWindow *m_window = nullptr;
    OpenGLFunction m_render;

};

#endif // SUNENGINEOPENGL_H
