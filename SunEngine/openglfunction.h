#ifndef OPENGLFUNCTION_H
#define OPENGLFUNCTION_H
#include <QOpenGLFunctions_3_3_Core>
#include <QColor>
#include <QTimer>

class OpenGLFunction:public QOpenGLFunctions_3_3_Core
{
public:
    OpenGLFunction();
    ~OpenGLFunction();
    void init();
    void paint();
    void resize(int w, int h);
    void setColor(QColor color);
    QColor m_color;
private:
    QTimer m_timer;
};

#endif // OPENGLFUNCTION_H
