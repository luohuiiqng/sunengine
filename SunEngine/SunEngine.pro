QT += quick gui

SOURCES += \
    assist.cpp \
    main.cpp \
    openglfunction.cpp \
    sunengineopengl.cpp

RESOURCES += Resource.qrc
CONFIG +=C++14
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    assist.h \
    openglfunction.h \
    sunengineopengl.h
