#include <QCoreApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include "assist.h"
#include "sunengineopengl.h"


int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QGuiApplication app(argc, argv);
    qmlRegisterType<Assist>("Assist",1,1,"Render");
    QQmlApplicationEngine engine;
    QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGLRhi);
    const QUrl url("qrc:/qml/main.qml");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
